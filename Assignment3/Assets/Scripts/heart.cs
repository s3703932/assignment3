﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heart : MonoBehaviour
{
    private SpriteRenderer spriteAnimate;
    public List<Sprite> heartSprites;

    private int counter;
    private int timer;

    // Start is called before the first frame update
    void Start()
    {
        spriteAnimate = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timer == 4)
        {
            Animate();
            timer = 0;
        }
        timer++;
    }

    private void Animate()
    {

        if (counter >= 8)
        {

            counter = 0;
        }
        spriteAnimate.sprite = heartSprites[counter];
        counter++;
    }
}
