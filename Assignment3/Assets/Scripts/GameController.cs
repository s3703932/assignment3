﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class GameController : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameController instance;

    public Text Score;
    public Text HP;
    public GameObject playerPF;
    public GameObject swapnPosition;
    //private Mapmanager mapManager;
    public GameObject levelimage;
    public GameObject gameover;
    public GameObject Invincible;
    public Text leveltext;
    int curScore;
    int curHP=3;
    int lifeCap=9;
    public int levelnum=1;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    IEnumerator Start()
    {
        SetUpUI(1);
        yield return new WaitForSeconds(3f);
        SetUpScene();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void SetUpUI(int i) {
        leveltext.text = "Level" + i;
        levelimage.SetActive(true);
    }

    void SetUpScene() {
        levelimage.SetActive(false);
        //EnemyManager.instance.Start();
        CreatePlayer();
        UpdateHP(0);
        

    }

    public void Updatescore(int score) {
        curScore += score;
        Score.text = curScore.ToString();
        
    }
    public void UpdateHP(int d)
    {
        if (curHP+d >= lifeCap) return;
        if (curHP + d <=0) {
            Gameover();
            gameover.SetActive(true);
            HP.text = "0";
            return;
        }
        if (d > 0) curHP++;
        if (d < 0) curHP--;
        HP.text = curHP.ToString();

    }

    public void CreatePlayer() {
        Instantiate(playerPF,swapnPosition.transform.position,Quaternion.identity);
        
    }

    public void LoadNextScene() {
        int nxtIndex = SceneManager.GetActiveScene().buildIndex + 1; 
        SceneManager.LoadScene(nxtIndex);
        SetUpUI(nxtIndex);
        Invoke("SetUpScene",3f);
        Setlevel(nxtIndex);
        SetIconFalse();
        //EnemyManager.instance.ResetZombie();
        //mapManager = GetComponent<Mapmanager>();
        //Mapmanager.instance.InitMap();

    }


    public void LoadStart() {
        SceneManager.LoadScene(0);

    }

    public void Quit() {
        Application.Quit();

    }

    void DestroyItself() {
        Destroy(gameObject);
        LoadStart();

    }
    void Gameover() {
        gameover.SetActive(true);
        Invoke("DestroyItself", 3f);

    }
    public void Setlevel(int nxtIndex) {
        levelnum = nxtIndex;

    }
    public void SetIconTrue() {
        Invincible.SetActive(true);

    }
    public void SetIconFalse()
    {
        Invincible.SetActive(false);

    }


}
