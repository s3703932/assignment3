﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : MonoBehaviour
{
    private const int RIGHT = 0, LEFT = 180;
    private SpriteRenderer spriteAnimate;
    public List<Sprite> batSprites;
    public List<Sprite> batSprites2;
    private Rigidbody2D rigidbody2D;
    private int counter;
    private int counter2;
    private int timer;
    private int timer2;
    private int nxtDirection=RIGHT;
    private float moveSpeed = 8f;
    private int enemyScore = 150;
    private float turnInterval;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        spriteAnimate = GetComponent<SpriteRenderer>();
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        CountDownAndTurn();
    }

    private void Animate()
    {

        if (counter >= 4)
        {

            counter = 0;
        }
        spriteAnimate.sprite = batSprites[counter];
        counter++;
    }
    private void Animate2()
    {

        if (counter2 >= 4)
        {

            counter2 = 0;
        }
        spriteAnimate.sprite = batSprites2[counter2];
        counter2++;
    }

    private void Move()
    {

        float xSpeed = 0f;
       

        switch (nxtDirection)
        {
            
            case LEFT:
                xSpeed = -1;
                if (timer == 4)
                {
                    Animate();
                    timer = 0;
                }
                timer++;
                break;
            case RIGHT:
                xSpeed = 1;
                if (timer2 == 4)
                {
                    Animate2();
                    timer2 = 0;
                }
                timer2++;
                break;


        }


        rigidbody2D.velocity = new Vector2(xSpeed, 0) * moveSpeed;

    }
    private void Turn() {
        if (nxtDirection == RIGHT) { nxtDirection = LEFT; return; }
        if (nxtDirection == LEFT) { nxtDirection = RIGHT;return; }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Turn();

    }
    public void Dealhit() {
        GameController.instance.Updatescore(enemyScore);
        rigidbody2D.simulated = false;
        Destroy(gameObject);
    }

    public void CountDownAndTurn()
    {

        turnInterval -= Time.deltaTime;
        if (turnInterval < 0)
        {
            Turn();
            turnInterval = Random.Range(2, 4);
        }

    }
}
