﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private const int  RIGHT = 0, LEFT = 180;
    private Rigidbody2D rigidbody2D;
    private Animator animator;

    [SerializeField]
    private GameObject projectilePF;

    public float beattackTimer;
    public float beattackTime;
    public float befireTimer;
    public float befireTime;
    
    private float moveSpeed = 3f;
    private float moveSpeedSet;

    [SerializeField]
    private int hp= 3;
    private int nxtDirection;
    private float last_speedX=0;
    private float last_speedY=0;
    private bool canbehurt = true;
    private bool canbefire = false;


    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

    }

    // Start is called before the first frame update
    void Start()
    {
        moveSpeedSet = moveSpeed;
        beattackTimer = 3;
        beattackTime = 3.0f;
        befireTimer = 3f;
        befireTime = 3f;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Turn();
        Fire();
        if (canbehurt==false) {
            if (beattackTimer > 0)
                beattackTimer -= Time.deltaTime;
            if (beattackTimer < 0)
                beattackTimer = 0;
            if (beattackTimer == 0)
            {
                GameController.instance.SetIconFalse();
                canbehurt = true;
                beattackTimer = beattackTime;
            }
            
        }
        GetFire();
    }

    private void Move()
    {

        var xSpeed = Input.GetAxis("Horizontal");
        var ySpeed = Input.GetAxis("Vertical");

        rigidbody2D.velocity = new Vector2(xSpeed, ySpeed) * moveSpeed;
        if (Mathf.Abs(xSpeed) > Mathf.Epsilon)
        {
            
            nxtDirection = xSpeed > 0 ? RIGHT : LEFT;
            animator.SetBool("ismoving", true);
            last_speedX = xSpeed;
            return;
            
        }
        else {
            animator.SetBool("ismoving", false);
        }
        
        if (Mathf.Abs(ySpeed) > Mathf.Epsilon)
        {
            
            animator.SetBool("ismoving", true);
            last_speedY = ySpeed;
            return;
        }
        else {
            animator.SetBool("ismoving", false);
           

        }




        animator.SetFloat("speed", last_speedX);
        animator.SetFloat("speed", last_speedY);

    }

    private void Turn()
    {
        float curDirection = transform.eulerAngles.y;
        transform.Rotate(0, nxtDirection - curDirection, 0);




    }

    private void Fire()
    {
      
       
        if (Input.GetButtonDown("Fire1"))
        {

            animator.SetTrigger("attack");
            Instantiate(projectilePF,transform.position,transform.rotation);
        }
        
    }

    

    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "enemy" && canbehurt == true)
        {
            Dealhit();


        }
        if (collision.gameObject.tag == "exit")
        {
            GameController.instance.LoadNextScene();

        }
        if (collision.gameObject.tag == "coin")
        {
            GameController.instance.Updatescore(50);
            Destroy(collision.gameObject);

        }
        if (collision.gameObject.tag == "heart")
        {
            GameController.instance.UpdateHP(1);
            if (hp <= 9) hp++;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "speed")
        {
            moveSpeedSet += 0.5f;
            moveSpeed = moveSpeedSet;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "mushroom")
        {
            GameController.instance.SetIconTrue();
            canbehurt = false;
            Invoke("Setcanbehurt",3.01f);
            Destroy(collision.gameObject);

        }
        if (collision.gameObject.tag == "bat" && canbehurt == true)
        {
            Dealhit();

        }
        if (collision.gameObject.tag == "ball" && canbehurt == true)
        {
            Dealhit();

        }

    }
    private void Setcanbehurt() {
        GameController.instance.SetIconTrue();
        canbehurt = false;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "lava")
        {
            moveSpeed = 1;

            canbefire = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "lava")
        {
            moveSpeed = moveSpeedSet;
            befireTimer = 3f;
            canbefire = false;
        }
    }

    
    private void GetFire() {
        if (canbefire == true) {
            
            if (befireTimer > 0) {
                befireTimer -= Time.deltaTime;
            }
            if (befireTimer < 0) {
                befireTimer = 0;
            }
            if (befireTimer == 0&&canbehurt==true) {
                Dealhit();
                befireTimer = befireTime;

            }

        }

    }


    //if (collision.gameObject.tag == "coin")
    //{
    //   sceneScrip.Score = sceneScrip.Score + 1;
    //   sceneScrip.onCoinCollected(collision.gameObject);
    //   Destroy(collision.gameObject);
    // }



    // if (collision.gameObject.tag == "portal")
    //{
    //    sceneScrip.onCharacterEnteringPortal();
    // }

    public void Dealhit()
    {
        GameController.instance.SetIconTrue();
        GameController.instance.UpdateHP(-1);
        canbehurt = false;
        --hp;
        animator.SetTrigger("hurt");
        if (hp== 0)
        {
            //GameController.instance.UpdateHP(-1);
            Destroy(gameObject);
            GameController.instance.UpdateHP(-1);
            rigidbody2D.simulated = false;

        }
        
    }

    


    public void Over()
    {
        
        animator.SetTrigger("over");
    }

    public void Attackover() {
        
        animator.SetTrigger("attackover");
        

    }

}