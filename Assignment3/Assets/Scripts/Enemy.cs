﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private const int RIGHT = 180, LEFT = 0,UP=360,DOWN=-180;
    private Rigidbody2D rigidbody2D;
    private Animator animator;
    [SerializeField]
    private int health = 1;

    [SerializeField]
    private GameObject projectilePF;
    [SerializeField]
    private int enemyScore=10;

    [SerializeField]
    private float moveSpeed = 10f;
    
    private int nxtDirection;

    private bool isAlive = false;
    

    [SerializeField]
    private float minTimeBeforeNextTurn = 0.5f;
    [SerializeField]
    private float maxTimeBeforeNextTurn = 3f;
    private float turnInterval = 1f;

    private void Awake()
    {
        turnInterval = Random.Range(minTimeBeforeNextTurn,maxTimeBeforeNextTurn);
        rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!isAlive) return;
        Move();
        CountDownAndTurn();
        Fire();

    }

    private void Move()
    {

        float xSpeed = 0f;
        float ySpeed = 0f;

        switch (nxtDirection) {
            case UP:
                ySpeed = 1;
                break;
            case DOWN:
                ySpeed = -1;
                break;
            case LEFT:
                xSpeed = -1;
                break;
            case RIGHT:
                xSpeed = 1;
                break;


        }


        rigidbody2D.velocity = new Vector2(xSpeed, ySpeed) * moveSpeed;


        

    }

    private void Turn()
    {

        int nxDirval = Random.Range(0,4);
        switch (nxDirval) {
            case 0:
                nxtDirection = RIGHT;
                break;
            case 1:
                nxtDirection = LEFT;
                break;
            case 2:
                nxtDirection = UP;
                break;
            case 3:
                nxtDirection = DOWN;
                break;
            default:
                nxDirval = RIGHT;
                break;

        }


        float curDirection = transform.eulerAngles.y;
        transform.Rotate(0, nxtDirection - curDirection, 0);




    }

    private void Fire()
    {
        //if (Input.GetButtonDown("Fire1"))
        //{
        //    Instantiate(projectilePF, transform.position, transform.rotation);

        // }
    }

    public void InitDone() {
        isAlive = true;
    }

    public void CountDownAndTurn() {

        turnInterval -= Time.deltaTime;
        if (turnInterval<0) {
            Turn();
            turnInterval = Random.Range(minTimeBeforeNextTurn, maxTimeBeforeNextTurn);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Turn();
        
    }

    public void Dealhit() {
        --health;
        if (health == 0) {
            isAlive = false;
            animator.SetTrigger("Dead");
            rigidbody2D.simulated = false;
            
        }
    }

    public void Destroy()
    {
        GameController.instance.Updatescore(enemyScore);
        Destroy(gameObject);
    }


}